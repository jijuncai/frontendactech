import { extend, localize } from "vee-validate";
import zh_cn from "vee-validate/dist/locale/zh_CN.json";
import * as Rules from 'vee-validate/dist/rules'

for (var rule in Rules) {
  extend(rule, Rules[rule])
}

localize("zh_CN", zh_cn)


