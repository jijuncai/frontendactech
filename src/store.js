import Vue from 'vue'
import Vuex from 'vuex'
import VueCookie from 'vue-cookie'
import { getAPI } from './axios-api'

Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        email: VueCookie.get('email') || null,
        username: VueCookie.get('username') || null,
        access_token: VueCookie.get('access_token') || null,
        users_list: null,
        events: null,
        projects: null,
    },
    mutations: {
        updateStorage (state, { email, username, access_token }) {
            VueCookie.set('email', email, 7)
            VueCookie.set('username', username, 7)
            VueCookie.set('access_token', access_token, 7)
            state.email = VueCookie.get('email')
            state.username = VueCookie.get('username')
            state.access_token = VueCookie.get('access_token')
        },
        destroyToken (state) {
            VueCookie.delete('email')
            VueCookie.delete('username')
            VueCookie.delete('access_token')
            state.email = null
            state.username = null
            state.access_token = null
        }

    },
    getters: {
        loggedIn (state) {
            return state.access_token != null
        }
    },
    actions: {
        userLogout (context) {
            if (context.getters.loggedIn) {
                context.commit('destroyToken')
            }
        },

        userLogin (context, credentials) {
            return new Promise((resolve, reject) => {
                getAPI.post('/auth/login/', {
                    email: credentials.email,
                    password: credentials.password
                })
                    .then(response => {
                        let jsonData = JSON.stringify(response.data)
                        console.log("userLogin里的原 " + jsonData)
                        context.commit('updateStorage', { email: response.data.email, username: response.data.username, access_token: response.data.access })
                        resolve()
                    })
                    .catch(err => {
                        reject(err)
                    })
            })
        },

        register (context, credentials) {
            return new Promise((resolve, reject) => {
                // VueProgressBar.start()
                // this.$Progress.start()
                getAPI.post('/auth/register/', {
                    email: credentials.email,
                    username: credentials.username,
                    password: credentials.password
                })
                    .then(response => {
                        // VueProgressBar.finish()
                        // this.$Progress.finish()
                        let jsonData = JSON.stringify(response.data)
                        console.log("注册成功 数据 " + jsonData)
                        resolve()
                    })
                    .catch(err => {
                        // VueProgressBar.fail()
                        // this.$Progress.fail()
                        reject(err)
                    })
            })
        }
    }
})