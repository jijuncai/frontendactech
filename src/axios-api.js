import axios from 'axios'

const getAPI = axios.create({
    baseURL: 'https://apis.actech.live',
    // baseURL: 'https://actech-env.eba-vmmkzevq.ap-northeast-1.elasticbeanstalk.com',
    // baseURL: 'http://127.0.0.1:8000',
    timeout: 5000,
})

export { getAPI }