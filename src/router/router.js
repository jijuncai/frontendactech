import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "@/components/mainpage/Home";



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/MainPage',
    component: Home,
    children:[
      {
        path: 'MainPage',
        name: 'MainPage',
        component: () => import(/* webpackChunkName: "main" */ '../components/mainpage/MainPage')
      },
      {
        path: 'about',
        name: 'about',
        component: () => import(/* webpackChunkName: "about" */ '../components/aboutcontact/About')
      },
      {
        path: 'JoinUs',
        name: 'JoinUs',
        component: () => import(/* webpackChunkName: "JoinUs" */ '../components/aboutcontact/JoinUs')
      },
      {
        path: 'contact',
        name: 'contact',
        component: () => import(/* webpackChunkName: "contact" */ '../components/aboutcontact/Contact')
      },
      {
        path: 'ProjectsPool',
        name: 'ProjectsPool',
        component: () => import(/* webpackChunkName: "ProjectsPool" */ '../components/projects/ProjectsPool')
      },
      {
        path: 'ProjectsService',
        name: 'ProjectsService',
        component: () => import(/* webpackChunkName: "ProjectsService" */ '../components/projects/ProjectsService')
      },
      {
        path: 'TalentsList',
        name: 'TalentsList',
        component: () => import(/* webpackChunkName: "TalentsPool" */ '../components/talents/TalentsList')
      },
      {
        path: 'TalentsPool',
        name: 'TalentsPool',
        component: () => import(/* webpackChunkName: "TalentsPool" */ '../components/talents/TalentsPool')
      },
      {
        path: 'TalentsService',
        name: 'TalentsService',
        component: () => import(/* webpackChunkName: "TalentsService" */ '../components/talents/TalentsService')
      },
      {
        path: 'TechTeamPool',
        name: 'TechTeamPool',
        component: () => import(/* webpackChunkName: "TechTeamPool" */ '../components/techteam/TechTeamPool')
      },
      {
        path: 'TechTeamService',
        name: 'TechTeamService',
        component: () => import(/* webpackChunkName: "TechTeamService" */ '../components/techteam/TechTeamService')
      },
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/login')
      },
      {
        path: 'logout',
        name: 'logout',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/logout')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/register')
      },
      {
        path: 'activate',
        name: 'activate',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/activate')
      },
      {
        path: 'profile',
        name: 'profile',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/profile')
      },
      {
        path: 'modifyProfile',
        name: 'modifyProfile',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/modifyProfile')
      },
      {
        path: 'requestResetEmail',
        name: 'requestResetEmail',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/requestResetEmail')
      },
      {
        path: 'passwordReset',
        name: 'passwordReset',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/passwordReset')
      },
      {
        path: 'termsAndPrivacy',
        name: 'termsAndPrivacy',
        component: () => import(/* webpackChunkName: "main" */ '../components/aboutcontact/termsAndPrivacy')
      },
      {
        path: 'CreateProject',
        name: 'CreateProject',
        component: () => import(/* webpackChunkName: "main" */ '../components/projects/CreateProject')
      },
      {
        path: 'resetPassword',
        name: 'resetPassword',
        component: () => import(/* webpackChunkName: "main" */ '../loginRegister/resetPassword')
      },
      {
        path: 'ModifyProject',
        name: 'ModifyProject',
        component: () => import(/* webpackChunkName: "main" */ '../components/projects/ModifyProject')
      },
      {
        path: 'projects',
        name: 'projects',
        component: () => import(/* webpackChunkName: "main" */ '../components/projects/projects')
      },
      {
        path: 'ProjectDetail',
        name: 'ProjectDetail',
        component: () => import(/* webpackChunkName: "main" */ '../components/projects/ProjectDetail')
      },
      {
        path: 'PublicProfile',
        name: 'PublicProfile',
        component: () => import(/* webpackChunkName: "main" */ '../components/talents/PublicProfile')
      },
      {
        path: 'TalentProfile',
        name: 'TalentProfile',
        component: () => import(/* webpackChunkName: "main" */ '../components/talents/TalentProfile')
      },
      {
        path: 'CreateEvent',
        name: 'CreateEvent',
        component: () => import(/* webpackChunkName: "main" */ '../components/events/CreateEvent')
      },
      {
        path: 'EventDetail',
        name: 'EventDetail',
        component: () => import(/* webpackChunkName: "main" */ '../components/events/EventDetail')
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router