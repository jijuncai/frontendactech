import Vue from 'vue';
import App from './App.vue';
import router from "@/router/router";
import store from "@/store";
import VueSimpleAlert from "vue-simple-alert";
import IdleVue from 'idle-vue';
import ToggleButton from 'vue-js-toggle-button';
import "./vee-validate";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import Paginate from 'vuejs-paginate';

const eventsHub = new Vue()

Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  idleTime: 600000
})

Vue.config.productionTip = false

Vue.use(VueSimpleAlert)
Vue.use(ToggleButton)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.component('paginate', Paginate)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
